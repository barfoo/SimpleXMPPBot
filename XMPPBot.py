#!/usr/bin/env python
 
import xmpp
import ConfigParser
import sys

class XmppBot(object):
 
    def __init__(self):
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read("xmppbot.cfg") 
        self.jid = xmpp.protocol.JID(self.cfg.get("bot", "jid"))
        self.password = self.cfg.get("bot", "password")
        self.room = self.cfg.get("bot", "room")
        self.owner = self.cfg.get("bot", "owner")
        self.client = xmpp.Client(self.jid.getDomain(), debug=[])
        self.con = self.client.connect()
        self.connect()
        while self.client.Process(1): 
            pass
     
    def connect(self):
        if not self.con:
            print "Could not connect"
            sys.exit()
        auth = self.client.auth(self.jid.getNode(), self.password, resource=self.jid.getResource())
        if not auth:
            print "Authentication failed"
            sys.exit()
        if self.con:
            self.client.sendInitPresence()    

if __name__ == "__main__":
    XmppBot = XmppBot()
